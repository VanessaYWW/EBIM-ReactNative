/**
 * Created by youweiwei on 2017/4/11.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import { StackNavigator } from 'react-navigation';

export default class ScreenTwo extends React.Component{

    static navigationOptions={
        title:'Two'
    }
    render(){
        const { params } = this.props.navigation.state;
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text>界面二</Text>
                <Text>{params.name}</Text>
            </View>
        )
    }
}