/**
 * Created by youweiwei on 2017/4/11.
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import { StackNavigator } from 'react-navigation';

export default class ScreenOne extends React.Component{
    static navigationOptions={
        title:'one'
    }
    render(){
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text onPress={()=>this.props.navigation.navigate('Two',{name:'vanessa'})}>界面一</Text>
            </View>
        )
    }
}
