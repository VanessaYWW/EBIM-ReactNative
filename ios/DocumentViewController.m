//
//  DocumentViewController.m
//  EBIM
//
//  Created by 尤维维 on 2017/4/6.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "DocumentViewController.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <React/RCTBridgeModule.h>

@interface DocumentViewController ()<RCTBridgeModule>

@end

@implementation DocumentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  NSURL *jsCodeLocation = [NSURL URLWithString:@"http://localhost:8081/ReactNative/Document/Document.bundle?platform=ios"];
  NSDictionary *dict = @{
                         @"projectId" : @"589d54acb9e9175385c086d9",
                         @"baseURL": @"https://test.ezbim.net"
                         };
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"EBIM"
                                               initialProperties:dict
                                                   launchOptions:nil];
  self.view = rootView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
