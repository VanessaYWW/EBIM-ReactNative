//
//  PwdViewController.m
//  EBIM
//
//  Created by 尤维维 on 2017/4/1.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "PwdViewController.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <React/RCTBridgeModule.h>

@interface PwdViewController ()<RCTBridgeModule>

@end

@implementation PwdViewController

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(back)
{
  self.navigationController.navigationBar.hidden = NO;
  [self.navigationController popViewControllerAnimated:YES];
}

RCT_EXPORT_METHOD(openImagePicker:(NSString *)userID
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  NSLog(@"打开相册");
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  NSURL *jsCodeLocation = [NSURL
                           URLWithString:@"http://localhost:8081/ReactNative/Regist/ForgetPwd.bundle?platform=ios"];
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"EBIM"
                                               initialProperties:nil
                                                   launchOptions:nil];
  self.view = rootView;
}

@end
