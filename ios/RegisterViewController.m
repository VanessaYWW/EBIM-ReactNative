//
//  RegisterViewController.m
//  EBIM
//
//  Created by 尤维维 on 2017/3/31.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "RegisterViewController.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <React/RCTBridgeModule.h>

@interface RegisterViewController ()<RCTBridgeModule>

@end

@implementation RegisterViewController
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(back)
{
  self.navigationController.navigationBar.hidden = NO;
  [self.navigationController popViewControllerAnimated:YES];
}

RCT_EXPORT_METHOD(openImagePicker:(NSString *)userID
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  NSLog(@"打开相册");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  NSURL *jsCodeLocation = [NSURL URLWithString:@"http://localhost:8081/ReactNative/Regist/MainRegist.bundle?platform=ios"];
//  NSURL *jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"regist.jsbundle" withExtension:nil];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"EBIM"
                                               initialProperties:nil
                                                   launchOptions:nil];
  self.view = rootView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
