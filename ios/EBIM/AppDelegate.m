/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"
#import "ViewController.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//  NSURL *jsCodeLocation = [NSURL
//                           URLWithString:@"http://localhost:8081/Account/load.bundle?platform=ios"];
//  
//  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
//                                                      moduleName:@"EBIM"
//                                               initialProperties:nil
//                                                   launchOptions:nil];
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  ViewController *rootViewController = [[ViewController alloc] init];
//  rootViewController.view = rootView;
//  self.window.rootViewController = rootViewController;
  UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:rootViewController];
  self.window.rootViewController = navVC;
  [self.window makeKeyAndVisible];
  return YES;
}

@end
