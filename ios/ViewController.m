//
//  ViewController.m
//  EBIM
//
//  Created by 尤维维 on 2017/3/31.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "ViewController.h"
#import "RegisterViewController.h"
#import "PwdViewController.h"
#import "DocumentViewController.h"
#import "DemoViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.view.backgroundColor = [UIColor whiteColor];
  
  // 登录
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://test.ezbim.net/api/v1/login?name=%@&password=%@",@"test",@"123456"]];
  NSURLRequest *request =[NSURLRequest requestWithURL:url];
  NSURLSession *session = [NSURLSession sharedSession];
  NSURLSessionDataTask *sessionDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
    NSLog(@"从服务器获取到数据");
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingMutableLeaves) error:nil];
  }];
  [sessionDataTask resume];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registClick
{
  self.navigationController.navigationBar.hidden = YES;
  RegisterViewController *resitVC = [[RegisterViewController alloc] init];
  [self.navigationController pushViewController:resitVC animated:YES];
}

- (IBAction)pwdClick
{
  self.navigationController.navigationBar.hidden = YES;
  PwdViewController *resitVC = [[PwdViewController alloc] init];
  [self.navigationController pushViewController:resitVC animated:YES];
}

- (IBAction)loadClick:(id)sender {
}

- (IBAction)documentClick:(id)sender {
  
  self.navigationController.navigationBar.hidden = YES;
  DocumentViewController *VC = [[DocumentViewController alloc] init];
  [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)demoClick:(id)sender {
  self.navigationController.navigationBar.hidden = YES;
  DemoViewController *VC = [[DemoViewController alloc] init];
  [self.navigationController pushViewController:VC animated:YES];
}


@end
