/**
 * Created by youweiwei on 2017/3/14.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Navigator,
} from 'react-native';

const Global = require('../Global');
const CommonStyle = require('../Common')

// 导航栏===(backAction,title,nextAction,rightItemTitle)
export default class NavBar extends React.Component{
    render(){
        return(
            <View style={[styles.body,CommonStyle.center,{height:this.props.hidden?0:64,paddingTop:this.props.hidden?0:20,}]}>
                {/*left item*/}
                <View style={[styles.leftItem,CommonStyle.justifyContentCenter]}>
                    <NavItem title={this.props.leftTitle} image={this.props.leftImage} action={this.props.leftAction}>
                    </NavItem>
                </View>
                {/*left item*/}
                <Text style={{fontSize: 17, fontWeight:'bold',color: Global.whiteColor}}>{this.props.title}</Text>
                {/*right item*/}
                <View style={[styles.rightItem]}>
                    <NavItem title={this.props.rightTitle} image={this.props.rightImage} action={this.props.rightAction}>
                    </NavItem>
                </View>
            </View>
        )
    }
}

class NavItem extends React.Component{
    render(){
        return(
            <TouchableOpacity
                style={{flexDirection:'row',alignItems:'center'}}
                onPress={this.props.action}>
                <Image source={this.props.image?this.props.image:null}></Image>
                <Text style={[this.props.style?this.props.style:styles.itemText]}>{this.props.title?this.props.title:null}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    body:{
        flexDirection:'row',
        backgroundColor: Global.themeColor,
    },
    leftItem:{
        flex:1,
        paddingLeft:15,
    },
    itemText:{
        color:Global.whiteColor,
        fontSize:17,
        fontWeight:'bold',
    },
    rightItem:{
        justifyContent:'flex-end',
        flex:1,
        marginRight:15,
        flexDirection:'row'
    }
})

module.exports = NavBar;