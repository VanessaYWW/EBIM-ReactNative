/**
 * Created by youweiwei on 2017/3/19.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    TouchableWithoutFeedback,
    Picker,
    Animated,
    Easing,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';

let Global = require('../Global');

var SimplePickerView = React.createClass({
    render(){
        var rows=[];
        var array = this.props.items;
        for(var i in array){
            var item=(
                <Picker.Item label={array[i]} value={array[i]} key={array[i]}/>
            )
            rows.push(item);
        }
        return(
            <View style={styles.body}>
                <TouchableWithoutFeedback onPress={this.props.hiddenPick}>
                    <View style={{flex:1}}></View>
                </TouchableWithoutFeedback>
                <Animated.View
                    style={{
                        height:200,
                        backgroundColor:'white',
                        transform: [{
                            translateY: this.state.fadeAnim.interpolate({
                                inputRange: [0, 1],
                                outputRange: [200, 0]  // 0 : 150, 0.5 : 75, 1 : 0
                            }),
                        }],}}>
                    {/*取消 确定*/}
                    <View style={styles.titleView}>
                        <Text onPress={()=>{this.props.hiddenPick()}}>取消</Text>
                        <Text onPress={()=>{
                            this.props.hiddenPick()
                            this.props.getValue(this.state.item)
                        }}>确定</Text>
                    </View>
                    <Picker
                        selectedValue={this.state.item}
                        onValueChange={(value)=>{
                            this.setState({
                                item:value
                            })
                        }}
                        style={{flex:1}}
                    >
                        {rows}
                    </Picker>
                </Animated.View>
            </View>
        )
    },
    getInitialState(){
        return{
            item:this.props.items[0]?this.props.items[0]:'',
            fadeAnim: new Animated.Value(0)
        }
    },
    componentDidMount(){
        Animated.timing(          // Uses easing functions
            this.state.fadeAnim,    // The value to drive
            {toValue: 1, duration: 200,},           // Configuration
        ).start();
    },
})

const styles = StyleSheet.create({
    body:{
        position: 'absolute',
        top: 0,
        left: 0,
        right:0 ,
        bottom:0,
        backgroundColor:'rgba(222, 222, 222, 0.3)',
        justifyContent: 'flex-end'
    },
    titleView:{
        flexDirection:'row',
        justifyContent:'space-between',
        height:35,
        alignItems:'center',
        borderBottomWidth:1,
        borderBottomColor:Global.grayColorc2,
        paddingLeft:20,
        paddingRight:20
    }
})


module.exports = SimplePickerView;