/**
 * Created by youweiwei on 2017/4/1.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    TouchableOpacity,
    Picker,
    Animated,
    Easing,
    View,
    Text,
} from 'react-native';

const Common = require('../Common')
let Global = require('../Global');

module.exports = {
    nextDiv(title,action){
        return(
            <TouchableOpacity onPress={action} activeOpacity={1}>
                <View style={[styles.next,Common.center]}>
                    <Text style={styles.text}>{title}</Text>
                </View>
            </TouchableOpacity>
        )
    }

}

const styles = StyleSheet.create({
    next:{
        backgroundColor: Global.themeColor,
        height: 50,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 30,
        borderRadius: 6,
    },
    text:{
        fontSize: 20,
        color: Global.whiteColor,
        fontWeight: 'bold'
    }
})