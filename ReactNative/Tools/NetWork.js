// 注册hearURL
let registHeadUrl = 'https://test.ezbim.net/api/v1/'
let Global = require('../Global');
module.exports = {
    // 获取图片地址
    getPictureUrl:function (pictureId) {
        var url = Global.baseURL + Global.baseLastString + 'pictures/' + pictureId;
        return url;
    },
    // get 请求
    fetchWork:function (url) {

        return fetch(url)
            .then(res=>res.json());

    },
    fetchPostWork:function (url,body) {
        fetch(url,{
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then(res=>res.json())
    },
    fetchPutWork:function (url,body) {
        return new Promise((resolve,reject)=>{
            fetch(url, {
                method: "PUT",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            })
                .then(res=>res.json())
                .then(data=>resolve(data))
                .catch(err=>reject(err))

        })
    },
    fetchPutWithData:function (url,formData) {
        return new Promise((resolve,reject)=>{
            fetch(url, {
                method: "PUT",
                body: formData,
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
                .then(res=>res.json())
                .then(data=>resolve(data))
                .catch(err=>reject(err))

        })
        // fetch(url, {
        //     method: "PUT",
        //     body: formData,
        //     headers: {
        //         'Content-Type': 'multipart/form-data'
        //     }
        // }).then(function(res) {
        //     if (res.ok) {
        //         res.json().then(function(data) {
        //             callBack(data);
        //         }, function(e) {
        //             failure(e)
        //         });
        //     } else if (res.status == 401) {
        //         failure('401');
        //     }
        // }, function(e) {
        //     failure(e)
        // });
    },
    // -----------------------------------------------城市/专业/行业--------------------------------
    getCities:function (callBack) {
        var cities = ['北京','上海','广州','深圳','南京'];
        callBack(cities);
    },
    getIndustries:function () {
        var url = registHeadUrl+'/system/industries';
        console.log(url);
        return this.fetchWork(url)
    },
    getProfessions:function () {
        var url = registHeadUrl+'/system/professions';
        console.log(url);
        return this.fetchWork(url)
    },
    // -----------------------------------------------注册-----------------------------------------
    // 1. 验证手机号是否存在(解析exists字段)
    verifyUserMobile:function (phone) {
        var url = registHeadUrl+'user_exists?mobile='+phone;
        console.log(url);
        return this.fetchWork(url)
    },

    // 2. 获取验证码(根据手机)
    postMobileVerify:function (phone) {
        var url = registHeadUrl+'sms'
        var body = {mobile:phone}
        return this.fetchPostWork(url,body)
    },
    // 3. 注册
    registerNewUserWithName:function (nickname,pwd,phone) {
        var url = registHeadUrl+'register'
        var body = {name:nickname,password:pwd,phoneNumber:phone}
        return this.fetchPostWork(url,body)
    },
    // 4. 修改个人页面
    putUserInfo:function (userID,attact) {
        var url = registHeadUrl+'users/'+userID
        return this.fetchPutWork(url,attact)
    },
    // 5. 更新头像
    sendUserAvatar:function (userID,avatar) {
        var url = registHeadUrl+'users/'+userID+'/avatar'
        var photo = {
            uri: avatar,
            type: 'image/jpeg',
            name: 'icon.jpg',
        };
        var data = new FormData()
        data.append('avatar', photo)
        return this.fetchPutWithData(url,data)
    },
    // 6. 修改密码
    resetPwd:function (userID,password) {
        var url = registHeadUrl + 'users/' + userID + '/reset_password'
        var attach = {
            password:password
        };
        return this.fetchPutWork(url,attach)
    },
    // -----------------------------------------------账号--------------------------------------------
    load:function (headUrl,account,pwd) {
        var url = headUrl + Global.baseLastString + 'login?name=' +account + '&password=' + pwd;
        console.log(url);
        return this.fetchWork(url)
        // this.fetchWork(url,callBack,failure);
    },
    // -----------------------------------------------项目相关-----------------------------------------
    // 1. 获取所有项目
    getAllProject:function () {
        var url = Global.baseURL + '/api/v1/projects';
        return this.fetchWork(url)
    },
    // 2. 获取项目成员
    getAllMembers:function (projectId) {
        var url = Global.baseURL + Global.baseLastString + 'projects/' + projectId + '/members?expand=true';
        return this.fetchWork(url)
    },

    // -----------------------------------------------话题相关-----------------------------------------
    // 1. 获取话题类型
    getTopicStates:function (projectId) {
        var url = Global.baseURL + Global.baseLastString + 'projects/' + projectId + '/topic_categories';
        return this.fetchWork(url)
    },
    // 2. 获取话题专业
    getTopicSystems:function (projectId) {
        var url = Global.baseURL + Global.baseLastString + 'projects/' + projectId + '/topic_system_types';
        return this.fetchWork(url)
    },
    // 获取所有话题
    getAllTopicS:function (lastUrl) {
        var url = Global.baseURL + '/api/v1/projects/' + Global.curProject.getProjectId() + lastUrl;
        return this.fetchWork(url)
    },
    // -----------------------------------------------资料相关-----------------------------------------
    // 1. 获取当前类型的资料
    getDocumentFile:function (projectId,attach) {
        var url = Global.baseURL + Global.baseLastString + 'projects/' + projectId + '/documents';
        attach!=null?url = url+'?'+attach:0;
        console.log(url)
        return this.fetchWork(url)
    },
    // 2. 上传文件夹(假数据)
    upLoadDocumentFolder(folderName){
        var key = parseInt(Math.random()*10000);
        var dict={name:folderName,id:key}
        return new Promise((resolve,reject)=>resolve(folderName))
    }

};

