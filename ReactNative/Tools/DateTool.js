/**
 * Created by youweiwei on 2017/2/20.
 */
import React from 'react';


module.exports = {
    // 处理服务器传来的日期格式
    handleDate:function(dateStr){
        var date = dateStr;
        var dt = new Date(Date.parse(date));
        dt.setMinutes(dt.getMinutes() - dt.getTimezoneOffset());
        var newdate = dt.toISOString().slice(0, -5).replace(/[T]/g, ' ');
        return newdate;
    },
    // xxxx-xx-xx格式
    handleDateWithDay:function (dateStr) {
        var location = dateStr.indexOf(' ');
        var res = dateStr.substr(0,location);
        return res;
    }
};


