import React from 'react';
import SQLiteStorage from 'react-native-sqlite-storage';

SQLiteStorage.DEBUG(true);
var database_name = "ezbim.db";
var database_version = "1.0";
var database_displayname = "MySQLite";
var database_size = -1;
var db;
const UserInfo_TABLE_NAME = "UserInfo";//当前项目下的人员
const Document_TABLE_NAME = "document";//当前项目下的人员

const SQLite = React.createClass({

    render(){
        return null;
    },
    componentWillUnmount(){
        if(db){
            this._successCB('close');
            db.close();
        }else {
            console.log("SQLiteStorage not open");
        }
    },
    open(){
        db = SQLiteStorage.openDatabase(
            database_name,
            database_version,
            database_displayname,
            database_size,
            ()=>{
                this._successCB('open');
            },
            (err)=>{
                this._errorCB('open',err);
            });
    },
    createTable(){
        if (!db) {
            this.open();
        }
        //创建资料表格
        db.transaction((tx)=> {
            tx.executeSql('CREATE TABLE IF NOT EXISTS ' + Document_TABLE_NAME + '(' +
                'id INTEGER PRIMARY KEY NOT NULL,' +
                'name VARCHAR,' +
                'fileId VARCHAR,' +
                'fileSize VARCHAR,' +
                'type VARCHAR'
                + ');'
                , [], ()=> {
                    this._successCB('executeSql');
                }, (err)=> {
                    this._errorCB('executeSql', err);
                });
        }, (err)=> {
            this._errorCB('transaction', err);
        }, ()=> {
            this._successCB('transaction');
        })
    },
    /*
     { name: '农村小别墅装修施工图.dwg',
     fileId: '58a6b132c94be463ca509881',
     fileType: 'dwg',
     fileSize: 7133221,
     suffix: 'dwg',
     fileName: '农村小别墅装修施工图.dwg',
     tag: '589d54acb9e9175385c086e9',
     projectId: '589d54acb9e9175385c086d9',
     generated: false,
     entity_uuids: [],
     entities: [],
     createdAt: '2017-02-17T08:16:03.903Z',
     createdBy: '589d23b44d03ab53de151ccd',
     updatedAt: '2017-02-17T08:16:03.903Z',
     updatedBy: '589d23b44d03ab53de151ccd',
     _id: '58a6b143c94be463ca50989e',
     frequent: false,
     creatorInfo:
     { name: '薛琳琳',
     nickname: '薛琳琳',
     avatar: '589d56d42719c1539b969893',
     _id: '589d23b44d03ab53de151ccd' } }
     */
    saveDocument(data,type){//保存该项目下所有成员的信息
        return new Promise((resolve, reject)=>{
            if(db){
                db.executeSql(
                    'INSERT INTO '+Document_TABLE_NAME+' (name,fileId,fileSize,type) VALUES(?,?,?,?)',
                    [data.name,data.fileId,data.fileSize,type],
                    ()=>{
                        this._successCB('saveDocument');
                        resolve('saveDocument_successCB');
                    },
                    (err)=>{
                        this._errorCB('saveDocument',err);
                        reject('saveDocument_errorCB');
                    })
            }else {
                reject('db not open');
            }
        });

    },
    getAllLocalDocumet(type){
        return new Promise((resolve, reject)=>{
            if(db){
                db.executeSql('SELECT * FROM '+Document_TABLE_NAME +' WHERE type=? LIMIT 100',[type],
                    (results)=>{
                        if(results.rows.length > 0){
                            var len = results.rows.length;
                            var datas = [];
                            for(let i=0;i<len;i++){
                                datas.push(results.rows.item(i));
                            }
                            resolve(datas);
                        }else {
                            reject('not find item');
                        }

                        this._successCB('findALl')
                    },(err)=>{
                        reject(err);
                        this._errorCB('findALl',err)
                    });
            }else {
                reject('db not open');
            }
        });
    },
    findDocument(fileId){//获取收藏记录
        return new Promise((resolve, reject)=>{
            if(db){
                db.executeSql('SELECT * FROM '+Document_TABLE_NAME +' WHERE fileId=? LIMIT 1',[fileId],
                    (results)=>{
                        if(results.rows.length > 0){
                            var len = results.rows.length;
                            var datas = [];
                            for(let i=0;i<len;i++){
                                datas.push(results.rows.item(i));
                            }
                            resolve(datas);
                        }else {
                            reject('not find item');
                        }

                        this._successCB('findUserInfoByUserId')
                    },(err)=>{
                        reject(err);
                        this._errorCB('findUserInfoByUserId',err)
                    });
            }else {
                reject('db not open');
            }
        });

    },
    deleteDocument(){//删除人员.db
        return new Promise((resolve, reject)=>{
            if(db){
                db.executeSql('DELETE FROM '+Document_TABLE_NAME ,
                    ()=>{
                        resolve();
                        this._successCB('deleteDocumentdb');
                    },(err)=>{
                        reject(err);
                        this._errorCB('deleteDocumentdb',err);
                    });
            }else {
                reject('db not open');
            }
        });

    },
    close(){
        if(db){
            this._successCB('close');
            db.close();
        }else {
            console.log("SQLiteStorage not open");
        }
        db = null;
    },
    _successCB(name){
        console.log("SQLiteStorage "+name+" success");
    },
    _errorCB(name, err){
        console.log("SQLiteStorage "+name+" error:"+err);
    }
});

module.exports = SQLite;

