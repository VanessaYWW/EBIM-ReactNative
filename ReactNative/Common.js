import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

const Globle = require('./Global')

var commonStyles = StyleSheet.create({
    flex: {
        flex: 1,
    },
    paddingTop64:{
        paddingTop: 64
    },
    justifyContentCenter:{
        justifyContent:'center'
    },
    alignItemsCenter:{
        alignItems:'center'
    },
    center:{
        justifyContent:'center',
        alignItems:'center'
    },
    paddingLeft15:{
        paddingLeft:15
    },
    fontSize17: {
        fontSize: 17
    },
    fontBold17: {
        fontSize: 17,
        fontWeight:'bold'
    },
    bottomBorder:{
        borderBottomColor:Globle.lightGrayColor,
        borderBottomWidth:1
    }
})

module.exports=commonStyles;
