/**
 * Created by youweiwei on 2017/3/29.
 */
import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    View,
    Image,
    ListView,
    TouchableWithoutFeedback,
} from 'react-native';

let Global = require('../Global');
let NetWork = require('../Tools/NetWork')
let DateTool = require('../Tools/DateTool')

export default class DownLoadFileView extends React.Component{
    render(){
        return(
            <View style={styles.body}>
                <View style={styles.showView}></View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    body:{
        position:'absolute',
        left:0,
        top:0,
        right:0,
        bottom:0,
        backgroundColor:'rgba(139, 139, 139, 0.3)',
        justifyContent:'center',
        alignItems:'center'
    },
    showView:{
        width:300,
        height:260,
        backgroundColor:Global.whiteColor,
        borderRadius:10,
    }
})

