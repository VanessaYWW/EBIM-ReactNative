/**
 * Created by youweiwei on 2017/3/25.
 * 资料管理界面
 */
import React, { Component } from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableWithoutFeedback,
    ScrollView,
    Navigator,
    Picker,
    DeviceEventEmitter
} from 'react-native';

import DownloadFileView from './DownLoadFile'
import DocumentAdd from './DocumentAdd'
import DocumentFolder from './DocumentFolder'
import NavBar from '../Custom/NavigationBar'

const Global = require('../Global');
const commonStyle = require('../Common')
const NetWork = require('../Tools/NetWork')
const DoucumentManager = require('react-native').NativeModules.Document
const DocumentList = require('./DocumentList')
const SQLite = require('../Tools/SQLite')

// 注册第一步,手机号+图片验证码
export default class MainView extends React.Component{
    configureScene() {
        return Navigator.SceneConfigs.PushFromRight; // 右侧弹出
    }
    renderScene(route, navigator) {
        return <route.component navigator={navigator}  {...route.passProps} />;
    }
    componentWillUnmount(){
    }
    componentWillMount(){
        console.log('资料\n'+this.props['baseURL']+'\n'+this.props["projectId"])
        Global.baseURL=this.props['baseURL']
        NetWork.load(Global.baseURL,'test','123456')
            .then((data)=>{
                if (data._id){
                    console.log('成功登录'+data)
                }
            })
            .catch(err=>console.log(err))
    }
    render() {
        return (
            <Navigator
                style={{flex:1}}
                initialRoute={{
                    component: Document,
                    passProps: {
                        projectId:this.props["projectId"]
                    },
                }}
                configureScene={this.configureScene}
                renderScene={this.renderScene}/>
        );
    }
}

var _scrollView;
var sqlite;
class Document extends React.Component{
    constructor(props){
        super(props);
        this.allItems=[];
        this.commonItems=[];
        this.graphItems=[];
        this.downloadItems=[];
        this.state={
            type:'all',
            isDownload:false,
            addHudShow:false,
        }
    }
    componentDidMount(){
        sqlite = new SQLite();
        sqlite.open();
        sqlite.createTable();
        // 获取所有的资料
        sqlite.getAllLocalDocumet('all')
            .then((res)=>{
                console.log('本地数据:成功\n'+res)
            },function (err) {
                console.log('本地数据:失败\n'+err)
            })


        //收到监听
        this.listener = DeviceEventEmitter.addListener('nextFolder',(folder)=>{
            this._openFolder(folder)
        });
    }
    componentWillUnmount(){
        // 移除监听
        this.listener.remove();
        sqlite.close();
    }

    _changeType(type){
        var page = 0;
         if (type=='common'){
             page = 1
         }else if (type=='graph'){
            page = 2
         }else if (type=='download'){
             page = 3
         }

         let X = page*Global.screem_width;
         _scrollView.scrollTo({y: 0,x:X,animation:false})
         this.setState({
             type:type,
         })
    }
    // 预览资料
    _openFile(file){
        alert(file.name+'\n'+file.fileId+'\n'+file.fileSize)
    }
    // 进入文件夹
    _openFolder(folder){
        // alert(folder.name+'\n'+folder.id)
        this.props.navigator.push({
            component:DocumentFolder,
            passProps:{
                projectId:this.props.projectId,
                folder:folder
            }
        })
    }
    render(){
        return(
            <View style={commonStyle.flex}>
                <NavBar hidden={false}
                        title="资料"
                        leftImage={require('../images/Common/button_return_white.png')}
                        rightImage={require('../images/Common/button_add_uncheck.png')}
                        leftAction={()=>{
                            //DoucumentManager.back()
                        }}
                        rightAction={()=>{
                            var addHudShow = this.state.addHudShow;
                            this.setState({
                                addHudShow:!addHudShow
                            })
                        }}
                />
                {/*内容*/}
                <ScrollView
                    ref={(scrollView) => { _scrollView==null?_scrollView = scrollView:0 }}
                    horizontal={true}
                    pagingEnabled={true}>
                    <DocumentList dStyle={styles.list}
                                  projectId={this.props.projectId}
                                  type="all"
                                  openFile={this._openFile.bind(this)}
                                  openFolder={this._openFolder.bind(this)}
                                  key="all"/>

                    <DocumentList dStyle={styles.list}
                                  projectId={this.props.projectId}
                                  type="common"
                                  openFile={this._openFile.bind(this)}
                                  openFolder={this._openFolder.bind(this)}
                                  key="common"/>

                    <DocumentList dStyle={styles.list}
                                  projectId={this.props.projectId}
                                  type="graph"
                                  openFile={this._openFile.bind(this)}
                                  openFolder={this._openFolder.bind(this)}
                                  key="graph"/>

                    <DocumentList dStyle={styles.list}
                                  projectId={this.props.projectId}
                                  type="download"
                                  openFile={this._openFile.bind(this)}
                                  openFolder={this._openFolder.bind(this)}
                                  key="download"/>
                </ScrollView>
                {/*类型选择标题栏*/}
                <HeadView changeType={this._changeType.bind(this)}/>
                {this.state.isDownload?<DownloadFileView/>:null}
                {this.state.addHudShow?
                    <DocumentAdd
                        dismiss={()=>{
                            this.setState({
                                addHudShow:false
                            })
                        }}
                        creatFolder={(folderTitle)=>{
                            NetWork.upLoadDocumentFolder(folderTitle)
                                .then(data=>{
                                    DeviceEventEmitter.emit(this.state.type,data); //发监听
                                    this.setState({
                                        addHudShow:false
                                    })
                                })
                                .catch(err=>console.log(err))
                        }}
                    />
                    :null}
            </View>
        )
    }
}


//四个按钮类型(all, common, graph, download)
class HeadView extends React.Component{
    constructor(props){
        super(props);
        this.state={
            type:'all'
        }
    }
    _changeType(type){
        this.setState({
            type:type,
        })
        if (this.props.changeType){
            this.props.changeType(type)
        }
    }
    render(){
        return(
            <ScrollView style={[styles.titleView]}
                        horizontal={true}>
                <Text
                    key="all"
                    style={[styles.title_text,{color:this.state.type=='all'?'black':Global.grayColor}]}
                    onPress={()=>this._changeType('all')}
                >全部</Text>
                <Text
                    key="common"
                    style={[styles.title_text,{color:this.state.type=='common'?'black':Global.grayColor}]}
                    onPress={()=>this._changeType('common')}
                >常用</Text>
                <Text
                    key="graph"
                    style={[styles.title_text,{color:this.state.type=='graph'?'black':Global.grayColor}]}
                    onPress={()=>this._changeType('graph')}
                >图纸</Text>
                <Text
                    key="download"
                    style={[styles.title_text,{color:this.state.type=='download'?'black':Global.grayColor}]}
                    onPress={()=>this._changeType('download')}
                >已下载</Text>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    flexRow:{
        flexDirection:'row'
    },
    alignItems_center:{
        alignItems:'center'
    },
    justifyContent_center:{
      justifyContent:'center'
    },
    titleView:{
        position:'absolute',
        height:44,
        left:0,
        right:0,
        top:64,
        backgroundColor:Global.whiteColor,
        borderBottomColor:Global.grayColorc2,
        borderBottomWidth:1,

    },
    title_text:{
        height:43,
        flex:1,
        width:Global.screem_width/4,
        textAlign:'center',
        paddingTop:15,
        borderBottomWidth:1,
        fontWeight:'bold',
        fontSize:17
    },
    list:{
        flex:1,
        height:Global.screem_height-64,
        width:Global.screem_width,
        paddingTop:44
    }
})


AppRegistry.registerComponent('EBIM', () => MainView);