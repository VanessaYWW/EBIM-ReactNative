/**
 * Created by youweiwei on 2017/4/6.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Animated,
    TouchableOpacity,
    Text,
    Image,
    TextInput,
    Alert
} from 'react-native';

const commonStyle = require('../Common')
const Global = require('../Global')

var dismissAction;
export default class AddDiv extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            fadeAnim: new Animated.Value(-1),
            showPanel:true,
            folder:false,
        };
    }
    componentWillMount(){
        if (dismissAction==null){
            dismissAction = this.props.dismiss
        }
    }
    componentDidMount(){
        Animated.timing(          // Uses easing functions
            this.state.fadeAnim,    // The value to drive
            {toValue: 0, duration: 200,},           // Configuration
        ).start();
    }
    _addPicture = ()=>{

    }
    _addVideo = ()=>{

    }
    _addNote = ()=>{

    }
    _addFolder = ()=>{
        this.setState({
            showPanel:false,
            folder:true,
        })
    }
    render(){
        return(
            <View style={[styles.bgView,{alignItems:this.state.folder?'center':null}]}>
                    <TouchableOpacity onPress={dismissAction}>
                    <View style={{alignItems:'flex-end',height:64}}>
                        {this.state.showPanel?
                            <Image
                                style={{width:52,marginTop:31,}}
                                source={require('../images/Common/button_add_uncheck.png')}
                                resizeMode="center">
                            </Image>:null}
                    </View>
                </TouchableOpacity>
                {/*展示按钮集面板*/}
                {this.state.showPanel?
                    <Animated.View
                        style={{
                            height:100,
                            backgroundColor:'white',
                            flexDirection:'row',
                            transform: [{
                                translateY: this.state.fadeAnim.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0, 64]
                                }),
                            }],}}>
                        <DButton title="图片" image={require('../images/Topic/button_topic_reply.png')}></DButton>
                        <DButton title="视频" image={require('../images/Report/icon_report_category.png')}></DButton>
                        <DButton title="笔记" image={require('../images/Topic/button_topic_reply.png')}></DButton>
                        <DButton title="文件夹" image={require('../images/Report/icon_report_category.png')} action={this._addFolder}></DButton>
                    </Animated.View>
                :null}
                {/*点击文件夹按钮后显示*/}
                {this.state.folder?
                    <FolderView creatFolder={this.props.creatFolder}>
                    </FolderView>
                :null
                }

            </View>
        )
    }
}

class DButton extends React.Component{
    render(){
        return(
            <TouchableOpacity onPress={this.props.action} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image source={this.props.image}></Image>
                <Text>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}

class FolderView extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            scaleAnim: new Animated.Value(0),
        };
    }
    componentDidMount(){
        Animated.timing(          // Uses easing functions
            this.state.scaleAnim,    // The value to drive
            {toValue: 1, duration: 200,},           // Configuration
        ).start();
    }
    render(){
        return(
            <Animated.View
                style={[
                    styles.folderView,
                    commonStyle.alignItemsCenter,
                    {transform:[
                        {scale: this.state.scaleAnim.interpolate({
                            inputRange: [0, 1],
                            outputRange: [0, 1],
                        })},
                    ]}]}>
                <Text style={{fontSize:17,fontWeight:'bold'}}>新建文件夹</Text>
                <TextInput
                    style={[styles.folderTextInput]}
                    placeholder={'请输入一个文件夹名称'}
                    placeholderTextColor={Global.grayColorc2}
                    autoFocus={true}
                    underlineColorAndroid = "transparent"  //android需要设置下划线为透明才能去掉下划线
                    onChangeText={(text)=>{
                        this.title=text
                    }}>
                </TextInput>
                <View style={{flexDirection:'row',marginTop:20,justifyContent:'space-around',width:300}}>
                    <Text key="cancle" style={[{color:Global.themeColor,},commonStyle.fontBold17]} onPress={dismissAction}>取消</Text>
                    <Text key="sure" style={[{color:Global.grayColor},commonStyle.fontBold17]}
                          onPress={()=>{
                              if (this.title==''){
                                  Alert.alert(
                                      '文件夹名称不可为空',
                                      '请输入正确的名称',
                                      [
                                          {text: '确定', onPress: null},
                                      ]
                                  )
                              } else {
                                this.props.creatFolder(this.title)
                              }
                          }}>确定</Text>
                </View>
            </Animated.View>
        )
    }
}

const styles = StyleSheet.create({
    bgView:{
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        backgroundColor:Global.grayColorA,
    },
    folderView:{
        borderRadius:10,
        width:300,
        height:150,
        backgroundColor:Global.whiteColor,
        paddingTop:20,
        marginTop:100
    },
    folderTextInput:{
        height:40,
        width:260,
        fontSize:15,
        borderRadius:3,
        borderColor:Global.grayColorc2,
        borderWidth:1,
        marginTop:20,
        marginLeft:20
    }
})

