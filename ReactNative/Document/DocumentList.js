/**
 * Created by youweiwei on 2017/3/28.
 * 资料列表
 */
import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    Image,
    ListView,
    ScrollView,
    TouchableOpacity,
    RefreshControl,
    DeviceEventEmitter
} from 'react-native';

const Global = require('../Global');
const commonStyle = require('../Common')
const NetWork = require('../Tools/NetWork')
const DateTool = require('../Tools/DateTool')
const DoucumentManager = require('react-native').NativeModules.Document
const SQLite = require('../Tools/SQLite')

var sqlite = new SQLite();
var DocumentList = React.createClass({
    getInitialState(){
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return{
            isRefreshing:false,
            dataSource: ds.cloneWithRows([]),
        };
    },
    componentWillMount(){
        this.items=null;
        this.folders=['123'];
        this.page=0;
        this.type=this.props.type;
        this.lastCount = 101;
        this.getNewItems();
    },
    componentDidMount(){
        //收到监听
        this.listener = DeviceEventEmitter.addListener(this.type,(data)=> {
            this.insertFolder(data)
        });
    },
    componentWillUnmount(){
        // 移除监听
        this.listener.remove();
    },
    getNewItems(){
        console.log('新数据...');
        this.page=0;
        this.items=[];
        this.setState({
            isRefreshing:true,
        })
        if (this.type=='download'){//本地数据
            setTimeout(()=>{
                this.setState({
                    isRefreshing:false
                })
            },2000);

        }else{//网络数据
            this.getDate();
        }

    },
    getMoreItems(){
        if (this.state.isRefreshing==true)return;
        if (this.page<1) return;
        // if (this.lastCount<100) return;
        // console.log('加载更多...')
        this.setState({
            isRefreshing:true
        })
        this.getDate();
    },
    getDate(){
        var _that = this;
        var attach = 'skip='+this.page++*100 + '&limit=100'
        switch (this.type){
            case 'common':attach += '&frequent=true';
            case 'graph':attach += '&dwg=true';
        }

        NetWork.getDocumentFile(this.props.projectId,attach)
            .then(data=>{
                console.log(data);
                this.lastCount = data.length;
                this.items = this.items.concat(data)
                this.saveToLocal(data)
                var res = this.folders.length!=0?this.folders.concat(this.items):this.items
                if (this.isMounted){
                    this.setState({
                        isRefreshing:false,
                        dataSource:this.state.dataSource.cloneWithRows(res)
                    })
                }
            }).catch(err=>{
                console.log(this.type+'err:'+err);
                this.setState({
                    isRefreshing:false,
                })
            })
    },
    // 保存到本地
    async saveToLocal(items){
        items.map((item,i)=>{
            sqlite.saveDocument(item,this.type)
                .then((res)=>{
                    
                },function (err) {
                    
                })
        })
    },
    // 新建文件夹
    insertFolder(data){
        if (this.folders[0]){
            var newFolders = [data].concat(this.folders);
            this.folders = newFolders
        }else {
            this.folders=[data]
        }
        var res = this.folders.concat(this.items)
        this.setState({
            dataSource:this.state.dataSource.cloneWithRows(res)
        })
    },
    // 进入下一文件夹
    _nextFolder(folder){
        this.props.openFolder(folder)
    },
    _renderRow(rowData,sectionID,rowID){
        return (
            <View style={commonStyle.bottomBorder}>
                {rowData.fileName?
                    // 资料cell
                    <View style={{paddingLeft:15,paddingRight:15,paddingTop:10,paddingBottom:10}}>
                        <Text style={{}}
                              onPress={()=>{
                                  this.props.openFile(rowData)
                              }}>{
                            rowData.fileName
                        }</Text>
                    </View> :
                    // 文件夹cell
                    (<TouchableOpacity onPress={this._nextFolder.bind(this,rowData)}>
                        <View style={[{flexDirection:'row'},commonStyle.alignItemsCenter]}>
                            <Image source={require('../images/Report/icon_report_category.png')}></Image>
                            <Text>{rowData}</Text>
                        </View>
                    </TouchableOpacity>)}

            </View>
        )
    },
    render(){
        return(
            <View style={this.props.dStyle}>
                <ListView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.getNewItems}
                            tintColor='black'
                            title="加载更多..."
                            titleColor='black'
                        />
                    }
                    dataSource={this.state.dataSource}
                    renderRow={this._renderRow}
                    onEndReached={this.getMoreItems}
                    initialListSize={12}
                    onEndReachedThreshold={10}
                    enableEmptySections={true}
                ></ListView>

            </View>
        )
    }
})

const styles = StyleSheet.create({
    body:{
        flex:1,
        height:Global.screem_height-64,
        width:Global.screem_width,
        paddingTop:44
    },
    cell:{
        borderBottomWidth:1,
        borderBottomColor:Global.lightGrayColor
    },
})

module.exports = DocumentList;