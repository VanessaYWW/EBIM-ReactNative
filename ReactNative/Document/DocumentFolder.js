/**
 * Created by youweiwei on 2017/4/7.
 * 资料文件夹
 */
import React, { Component } from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    View,
    Image,
    TouchableWithoutFeedback,
    ScrollView,
    Navigator,
    Picker,
    DeviceEventEmitter
} from 'react-native';

import NavBar from '../Custom/NavigationBar'

const Global = require('../Global');
const commonStyle = require('../Common')
const NetWork = require('../Tools/NetWork')
const DoucumentManager = require('react-native').NativeModules.Document
const DocumentList = require('./DocumentList')
const DownloadFileView = require('./DownLoadFile')
import DocumentAdd from './DocumentAdd'

export default class FolderView extends React.Component{
    constructor(props){
        super(props);
        this.folder = this.props.folder
        this.projectId = this.props.projectId
        this.state={
            addHudShow:false
        }
    }
    // 预览资料
    _openFile=(file)=>{
        alert(file.name+'\n'+file._id)
    }

    // 进入文件夹
    _openFolder=(folder)=>{
        // alert(folder.name+'\n'+folder.id+'\n'+this.projectId)
        DeviceEventEmitter.emit('nextFolder',folder); //发监听
    }
    render() {
        return (
            <View style={[commonStyle.flex,{backgroundColor:Global.whiteColor}]}>
                <NavBar hidden={false}
                        title={this.folder}
                        leftImage={require('../images/Common/button_return_white.png')}
                        rightImage={require('../images/Common/button_add_uncheck.png')}
                        leftAction={()=>{
                            this.props.navigator.pop()
                        }}
                        rightAction={()=>{
                            var addHudShow = this.state.addHudShow;
                            this.setState({
                                addHudShow:!addHudShow
                            })
                        }}
                />
                <DocumentList dStyle={styles.list}
                              projectId={this.props.projectId}
                              type={this.folder}
                              openFile={this._openFile}
                              openFolder={this._openFolder}
                />
                {this.state.addHudShow?
                    <DocumentAdd
                        dismiss={()=>{
                            this.setState({
                                addHudShow:false
                            })
                        }}
                        creatFolder={(folderTitle)=>{
                            NetWork.upLoadDocumentFolder(folderTitle)
                                .then(data=>{
                                    DeviceEventEmitter.emit(this.folder,data); //发监听
                                    this.setState({
                                        addHudShow:false
                                    })
                                })
                                .catch(err=>console.log(err))
                        }}
                    />
                    :null}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    list:{
        flex:1,
        height:Global.screem_height-64,
        width:Global.screem_width,
    }
});

