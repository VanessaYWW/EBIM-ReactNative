import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    Navigator
} from 'react-native';

const Global = require('../Global')
const commonStyles = require('../Common')
const NavBar = require('../Custom/NavigationBar')
const Custom = require('../Custom/CustomDiv')
const NetWork = require('../Tools/NetWork')
const Regist = require('../Regist/Register');
// import MainTab from './MainTab';

export default MainView = React.createClass({
    /**
     * 配置场景动画
     * @param route 路由
     * @param routeStack 路由栈
     * @returns {*} 动画
     */

    configureScene() {
        return Navigator.SceneConfigs.PushFromRight; // 右侧弹出
    },
    /**
     * 使用动态页面加载
     * @param route 路由
     * @param navigator 导航器
     * @returns {XML} 页面
     */
    renderScene(route, navigator) {
        return <route.component navigator={navigator}  {...route.passProps} />;
    },
    componentWillUnmount(){
    },

    render() {
        return (
            <Navigator
                style={{flex:1}}
                initialRoute={{
                    component: Load
                }}
                configureScene={this.configureScene}
                renderScene={this.renderScene}/>
        );
    }
})

var Load = React.createClass({
    getInitialState(){
        this.account='尤维维';
        this.pwd='yww123';
        this.url1='http://cloud.ezbim.net';
        this.url2='http://180.168.170.198:3000';
        this.url3='https://test.ezbim.net';
        return{
            urlType: 'url1',
            isloading: false,
        }
    },
    // 切换URL
    _urlChangeClick:function (urlType) {
        this.setState({
            urlType: urlType,
        })
    },
    // 登录
    _load:function () {//192.168.1.64:3000/api/v1/login?name=xxx&password=xxx
       if (this.state.isloading ){
           return;
       } else {
           this.setState({
               isloading: true
            })
           var account = this.account;
           var pwd = this.pwd;
           var curURL = this.state.urlType=='url1'?this.url1:(this.state.urlType=='url2'?this.url2:this.url3)
           NetWork.load(curURL,this.account,this.pwd,(data)=>{
               if (data._id){
                   console.log('成功登录'+data)
                   Global.baseURL = curURL
                   Global.user = jsonData
                   this.setState({
                       isloading: false
                   })
               }else {
                   this.setState({
                       isloading: false
                   })
               }
           },(err)=>{
               this.setState({
                   isloading: false
               })
           })
       }
    },

    // 游客登录
    _commonLoad(){

    },

    // 注册
    _regist(){
        this.props.navigator.push({
            component:Regist,
            passProps:{
                type:"regist"
            }
        })
    },
    // 忘记密码
    _forgetPwd(){
        this.props.navigator.push({
            component:Regist,
            passProps:{
                type:"forgetPwd"
            }
        })
    },

    render(){
        return(
            <View style={[commonStyles.flex]}>
                <NavBar title="登录"
                        hidden={false}
                        rightTitle="注册"
                        rightAction={this._regist}/>
                {/*账号*/}
                <TextDiv image={require('../images/Login/icon_user.png')}
                         placeholder="请输入账号"
                         value={this.account}
                         changeValueAction={(text)=>{this.account=text;}}/>
                {/*密码*/}
                <TextDiv image={require('../images/Login/icon_password.png')}
                         placeholder="请输入密码"
                         value={this.pwd}
                         changeValueAction={(text)=>{this.pwd=text;}}/>
                {/*网址*/}
                <View style={[styles.url,styles.grayBG]}>
                    {/*url1*/}
                    <UrlDiv choose={this.state.urlType=='url1'}
                            urlValue={this.url1}
                            title="公有云"
                            clickAction={this._urlChangeClick.bind(this,'url1')}
                            changeTextAction={(text)=>{this.url1=text}}/>

                    {/*url2*/}
                    <UrlDiv choose={this.state.urlType=='url2'}
                            urlValue={this.url2}
                            title="私有云1"
                            clickAction={this._urlChangeClick.bind(this,'url2')}
                            changeTextAction={(text)=>{this.url2=text}}/>

                    {/*url3*/}
                    <UrlDiv choose={this.state.urlType=='url3'}
                            urlValue={this.url3}
                            title="私有云2"
                            clickAction={this._urlChangeClick.bind(this,'url3')}
                            changeTextAction={(text)=>{this.url3=text}}/>

                </View>
                {/*登录*/}
                {Custom.nextDiv(this.state.isloading?'登录中...':'登录',this._load)}
                <View  style={[{flexDirection:'row',height:60,justifyContent:'space-between'},commonStyles.alignItemsCenter]}>
                    <Text style={{paddingLeft:40}} onPress={this._commonLoad}>游客登录</Text>
                    <Text style={{paddingRight:40}} onPress={this._forgetPwd}>忘记密码?</Text>
                </View>
                {/*logo*/}
                <View style={[commonStyles.flex,{justifyContent:'flex-end', marginBottom: 30}]}>
                    <Text style={styles.logo}>译筑信息科技(上海)有限公司</Text>
                </View>
            </View>
        )
    }
})

const TextDiv = React.createClass({
    render(){
        return(
            <View style={[styles.account,styles.grayBG]}>
                <Image source={this.props.image}></Image>
                <TextInput
                    placeholder={this.props.placeholder}
                    clearButtonMode={'while-editing'}
                    style={[commonStyles.flex]}
                    defaultValue={this.props.value}
                    onChangeText={this.props.changeValueAction}></TextInput>
            </View>
        )
    }
})

const UrlDiv = React.createClass({
    render(){
        return(
            <View style={[styles.urlbutton]}>
                <TouchableOpacity onPress={this.props.clickAction} activeOpacity={1}>
                    <Image
                        style={styles.header_button}
                        source={this.props.choose?require('../images/Login/icon_chosen@2x.png'):require('../images/Login/icon_unchosen@2x.png')}
                    />
                </TouchableOpacity>
                <TextInput style={[commonStyles.flex]}
                           defaultValue={this.props.urlValue}
                           editable={this.props.title=="公有云"?false:(this.props.choose?true:false)}
                           onChangeText={(text)=>{this.props.changeTextAction(text)}}></TextInput>
                <Text style={[styles.text]}>{this.props.title}</Text>
            </View>
        )
    }
})


const styles = StyleSheet.create({
    grayBG:{
        backgroundColor: Global.grayColorc2,
        borderRadius: 6,
    },
    account:{
        marginTop:20,
        marginLeft:30,
        marginRight:30,
        height:50,
        flexDirection:'row',
        alignItems:'center'
    },
    url:{
        marginTop:20,
        marginLeft:30,
        marginRight:30,
        height:150,
    },
    urlbutton: {
        flexDirection:'row',
        height:50,
        alignItems:'center',
    },
    text:{
        fontSize:17,
        marginRight:10
    },
    loadbutton:{
        height: 50,
        borderRadius: 6,
        backgroundColor:Global.themeColor,
        justifyContent:'center'
    },
    loadbutton_text:{
        textAlign:'center',color:Global.whiteColor, fontSize: 20, fontWeight:'bold'
    },
    loadbutton_demo:{
        height: 50,
        justifyContent:'center'
    },
    logo:{
        fontSize: 11,
        textAlign:'center',

    }

})


AppRegistry.registerComponent('EBIM', () => MainView);