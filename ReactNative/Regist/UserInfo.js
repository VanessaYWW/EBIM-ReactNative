/**
 * Created by youweiwei on 2017/3/23.
 * 个人完善资料
 */
import React, { Component } from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableWithoutFeedback,
    ScrollView,
    Navigator,
    Picker,
    NativeAppEventEmitter
} from 'react-native';

const Global = require('../Global');
const NetWork = require('../Tools/NetWork')
const NavBar = require('../Custom/NavigationBar')
const SimpleCityPickerView = require('../Custom/SimplePickerView')
const CustomDiv = require('../Custom/CustomDiv')

const RegisterViewController = require('react-native').NativeModules.RegisterViewController;

var o2cFun = NativeAppEventEmitter.addListener(
    'sendImage',
    (para) => {
        Alert.alert('被OC触发','字典数据:\n avatarPath:'+para.avatarPath)
        onsole.log('被OC触发','字典数据:\n avatarPath:'+para.avatarPath)
    })
// 4.完善个人信息
var PersonInfo = React.createClass({
    getInitialState(){
        return {
            avatarSource:null,
            city:'',
            trade:'',
            major:'',
            citys:null,
            trades:null,
            majors:null,
            pickType:'',
            pickOpen:false
        }
    },
    componentWillMount(){
        console.log(this.props.userData)
        NetWork.getCities((citys)=>{
            NetWork.getProfessions((majors)=>{
                NetWork.getIndustries((trades)=>{
                    this.setState({
                        citys:citys,
                        majors:majors,
                        trades:trades
                    })
                })
            })
        })
    },
    componentWillUnmount(){
        o2cFun.remove();
    },
    /*保存个人信息*/
    _save:function () {
        var attach = {
            nickname:this.state.nickname,
            email:this.state.email,
            location:this.state.city,
            profession:this.state.major,
            industry:this.state.trade
        };
        console.log(JSON.stringify(attach))
        NetWork.putUserInfo(this.props.userData._id,attach,(data)=>{
            console.log('保存成功')
            NetWork.sendUserAvatar(this.props.userData._id,this.state.avatarSource,(res)=>{
                console.log('保存头像成功')
                RegisterViewController.back()
            },(e)=>{
                console.log(e)
            })
        },(err)=>{
            console.log(err)
        })
    },
    /*点击选择器*/
    _openPick:function (type) {
        this.setState({
            pickType:type,
            pickOpen:true
        })
    },
    render(){
        return(
            <View style={styles.getCodeBody}>
                <NavBar title={'完善个人信息'}
                        rightAction={()=>{RegisterViewController.back()}} rightTitle='跳过'
                        leftImage={require('../images/Common/button_return_white.png')}
                        leftTitle="登录"
                        leftAction={()=>{
                            this.props.navigator.popToTop()
                        }}/>
                <ScrollView style={{flex:1}}>
                    {/*头像*/}
                    <View style={styles.pIconView}>
                        <TouchableWithoutFeedback onPress={()=>{
                            RegisterViewController.openImagePicker(this.props.userData._id)
                                .then((data)=>{
                                    let picUri = 'file://'+data
                                this.setState({
                                    avatarSource:picUri
                                })
                            }).catch((e)=>{
                                console.log(e)
                            })

                        }}>
                            <Image
                                source={this.state.avatarSource?{uri:this.state.avatarSource}:require('../images/Login/button_register_camera.png')}
                                style={styles.pIconImage}></Image>
                        </TouchableWithoutFeedback>
                    </View>
                    {/*cells*/}
                    <PCellTextInput name="昵称" placeholder="请输入您的昵称" changeText={(text)=>{
                        this.setState({
                            nickname:text
                        })
                    }}/>
                    <PCellTextInput name="邮箱" placeholder="请输入您的邮箱" changeText={(text)=>{
                        this.setState({
                            email:text
                        })
                    }}/>
                    <PCellText name="城市" detail={this.state.city} default="点击获取地区"
                               clickCell={this._openPick.bind(null,'city')}/>
                    <PCellText name="行业" detail={this.state.trade} default="点击获取行业"
                               clickCell={this._openPick.bind(null,'trade')}/>
                    <PCellText name="专业" detail={this.state.major} default="点击获取专业"
                               clickCell={this._openPick.bind(null,'major')}/>
                    {/*保存*/}
                    {CustomDiv.nextDiv('保存',this._save)}
                </ScrollView>
                {/*picker*/}
                {this.state.pickOpen?
                    <SimpleCityPickerView
                        hiddenPick={()=>{this.setState({pickOpen:false})}}
                        items={this.state.pickType=='city'?this.state.citys:(this.state.pickType=='major'?this.state.majors:this.state.trades)}
                        getValue={(value)=>{
                            if (this.state.pickType=='city'){
                                this.setState({
                                    city:value
                                })
                            }else if(this.state.pickType=='major'){
                                this.setState({
                                    major:value
                                })
                            }else {
                                this.setState({
                                    trade:value
                                })
                            }

                        }}></SimpleCityPickerView>
                    :null}
            </View>
        )
    }
})

var PCellText = React.createClass({
    render(){
        return(
            <TouchableWithoutFeedback onPress={this.props.clickCell}>
                <View style={styles.pCellView}>
                    <Text style={styles.pCellName}>{this.props.name}</Text>
                    <Text
                        style={[styles.pCellText,{color:!this.props.detail?Global.grayColor:'#000000'}]}
                    >{this.props.detail?this.props.detail:this.props.default}
                    </Text>
                    <Image
                        style={styles.pCellNextImage}
                        source={require('../images/Common/icon_next.png')}
                    ></Image>
                </View>
            </TouchableWithoutFeedback>
        )
    }
})
var PCellTextInput = React.createClass({
    render(){
        return(
            <View style={styles.pCellView}>
                <Text style={styles.pCellName}>{this.props.name}</Text>
                <TextInput
                    style={{flex:1,marginRight:10}}
                    underlineColorAndroid="transparent"
                    placeholder={this.props.placeholder}
                    clearButtonMode='while-editing'
                    onChangeText={this.props.changeText}
                ></TextInput>
            </View>
        )
    }
})

const styles = StyleSheet.create({
    fontSize17: {
        fontSize: 17
    },
    getCodeBody: {
        flex: 1,
        backgroundColor: Global.whiteColor
    },
    pIconView: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 160,
    },
    pIconImage: {
        width: 80,
        height: 80,
        borderRadius:40,
    },
    pCellView: {
        flexDirection: 'row',
        height: 44,
        marginLeft: 15,
        borderBottomWidth: 1,
        borderBottomColor: Global.lightGrayColor,
        alignItems: 'center',
    },
    pCellName: {
        fontSize: 17,
        marginRight: 15,
    },
    pCellNextImage: {
        marginRight: 10,
        marginLeft: 10,
        resizeMode: 'center'
    },
    pCellText: {
        flex: 1,
        fontSize: 17
    },
    next: {
        backgroundColor: Global.themeColor,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15,
        marginTop: 30,
        borderRadius: 6,
    }
});

module.exports = PersonInfo;