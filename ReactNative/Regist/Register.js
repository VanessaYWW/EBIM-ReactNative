import React, { Component } from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Navigator,
    Picker
} from 'react-native';

const Global = require('../Global');
const Common = require('../Common')
const NetWork = require('../Tools/NetWork')
const NavBar = require('../Custom/NavigationBar')
const TestCode = require('./TestCode')
const TipView = require('./TipView')
const PersonInfo = require('./UserInfo')
const CustomDiv = require('../Custom/CustomDiv')
const PwdViewController = require('react-native').NativeModules.PwdViewController;
const RegisterViewController = require('react-native').NativeModules.RegisterViewController;


/*------------------------------------------界面------------------------------------------*/
/*forgetPwd  regist*/
// 1.填写手机号+验证码
var Register = React.createClass({
    getInitialState(){
        return{
            precode: null,
            phone:'18961066160',
            writeCode: null,//测试用,获取验证码自动填写
        }
    },
    componentWillMount(){
        var newCode = this.getCodeString();
        this.setState({
            precode: newCode,
            writeCode: newCode,
            code:newCode,
        })
    },
    // 获取验证码
    getCodeString:function () {
        var newCode = '';
        var codeLength = 4;//验证码的长度
        var selectChar = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');//所有候选组成验证码的字符，当然也可以用中文的

        for (var i = 0; i < codeLength; i++) {
            var charIndex = Math.floor(Math.random() * selectChar.length);
            newCode += selectChar[charIndex];
        }
        return newCode;
    },
    // 点击下一步
    nextStep:function () {
        if (this._codeCanUse() == true){
            if (this._phoneCanUse() == true){
                NetWork.verifyUserMobile(this.state.phone,(jsonData)=>{
                    if (jsonData.exists == false){
                            if (this.props.type=='regist'){
                                this.props.navigator.push({
                                    component: TestCode,
                                    passProps: {
                                        name: '注册',
                                        phone:this.state.phone,
                                        type:'regist'
                                    },
                                })
                            }else {
                                Alert.alert(
                                    '您输入的手机号未注册!',
                                    '请返回注册界面注册',
                                    [
                                        {text: '确定', onPress:null},
                                    ]
                                )
                            }
                    }else {
                            if (this.props.type=='regist'){
                                Alert.alert(
                                    '此号码已注册!',
                                    '请重新输入',
                                    [
                                        {text: '确定', onPress:null},
                                    ]
                                )
                            }else {
                                this.props.navigator.push({
                                    component: TestCode,
                                    passProps: {
                                        name: '找回密码',
                                        phone:this.state.phone,
                                        type:'forgetPwd',
                                        user:jsonData
                                    },
                                })
                            }

                    }
                },(error)=>{
                    alert(error)
                })

            }else {
                Alert.alert(
                    '您输入的手机号有误!',
                    '请重新输入',
                    [
                        {text: '确定', onPress:null},
                    ]
                )
            }
        }else {
            Alert.alert(
                '您输入的验证码有误!',
                '请重新输入',
                [
                    {text: '确定', onPress:null},
                ]
            )
        }

    },

    // 验证验证码是否正确
    _phoneCanUse:function () {
        if (this.state.phone.length == 11){
            return true;
        }else {
            return false;
        }
    },

    // 验证手机号正确
    _codeCanUse:function () {
        if (this.state.precode.toLowerCase() == this.state.code.toLowerCase()){
            return true;
        }else {
            return false;
        }
    },

    // 返回
    _back(){
        this.props.type=='regist'?RegisterViewController.back():PwdViewController.back()
    },

    render(){
        return(
            <View style={[Common.flex,{backgroundColor:Global.whiteColor}]}>
                <NavBar hidden={false}
                        leftAction={this._back}
                        title={this.props.type=='regist'?'注册':'找回密码'}
                        leftTitle="登录"
                        leftImage={require('../images/Common/button_return_white.png')}/>
                {/*手机号*/}
                {PhoneDiv(this.props.type=='regist'?'手机号':'登录名','请输入手机号',this.state.phone,(text)=>{
                    this.setState({
                        phone: text
                    })
                })}
                {/*验证码*/}
                {CodeDiv(this.state.writeCode?this.state.writeCode:null,this.state.precode,(text)=>{
                    this.setState({
                        code: text
                    })
                })}
                {/*下一步*/}
                {CustomDiv.nextDiv('下一步',this.nextStep.bind(this,null))}
                {/*协议*/}{this.props.type=='regist'?<TipView></TipView>:null}
            </View>
        )
    }
})

const PhoneDiv = ((title,placeholderString,defaultString,changeValueAction)=>{
    return (
        <View style={[styles.cell,Common.bottomBorder,Common.alignItemsCenter]}>
            <Text style={[Common.fontSize17]}>{title}</Text>
            <TextInput placeholder={placeholderString}
                       style={[Common.flex, styles.text,]}
                       underlineColorAndroid="transparent"
                       keyboardType='numeric'
                       clearButtonMode={'while-editing'}
                       onChangeText={changeValueAction}
                       defaultValue={defaultString}
            ></TextInput>
        </View>
    )
})

const CodeDiv = ((value,code,changeValueAction)=>{
    return (
        <View style={[Common.alignItemsCenter,styles.cell,Common.bottomBorder]}>
            <Text style={[Common.fontSize17]}>验证码</Text>
            <TextInput placeholder={'请输入验证码'}
                       underlineColorAndroid="transparent"
                       style={[Common.flex, styles.text]}
                       clearButtonMode={'while-editing'}
                       onChangeText={changeValueAction}
                       value={value}></TextInput>
            <Text style={styles.code}>{code}</Text>
        </View>
    )
})



const styles = StyleSheet.create({
    cell: {
        flexDirection: 'row',
        height: 60,
        paddingRight: 15,
        paddingLeft: 15,
    },
    text: {
        paddingLeft: 15,
        paddingRight: 15,
    },
    code: {
        width: 80,
        backgroundColor: Global.lightGrayColor,
        height: 30,
        textAlign: 'center',
        paddingTop: 5,
    },
    next: {
        backgroundColor: Global.themeColor,
        height: 50,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 30,
        borderRadius: 6,
    }
})

module.exports = Register;
