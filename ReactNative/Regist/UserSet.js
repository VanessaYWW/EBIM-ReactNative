/**
 * Created by youweiwei on 2017/3/23.
 * 设置用户名+密码
 */
import React, { Component } from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableWithoutFeedback,
    ScrollView,
    Navigator,
    Picker,
    NativeAppEventEmitter
} from 'react-native';

const Global = require('../Global');
const NetWork = require('../Tools/NetWork')
const NavBar = require('../Custom/NavigationBar')
const PersonInfo = require('./UserInfo')
const CustomDiv = require('../Custom/CustomDiv')


// 3.注册:用户名+密码
var UserSet = React.createClass({
    getInitialState(){
        return{
            showPwd: false
        }
    },
    _register:function () {
        NetWork.registerNewUserWithName(this.state.name,this.state.pwd,this.props.phone,(data)=>{
            console.log('注册成功:\n'+data+data.name+data.password+data._id)
            // 注册成功
            this.props.navigator.push({
                component:PersonInfo,
                passProps: {
                    userData:data
                },
            })
        },(error)=>{console.log(error)})

    },
    render(){
        return(
            <View style={styles.getCodeBody}>
                <NavBar title={this.props.name}
                        leftImage={require('../images/Common/button_return_white.png')}
                        leftTitle="登录"
                        leftAction={()=>{
                            this.props.navigator.popToTop()
                        }}/>
                {/*用户名*/}
                <View style={styles.titleView}>
                    <Text style={[styles.fontSize17,{width: 60}]}>用户名</Text>
                    <TextInput
                        style={[styles.userNameText,styles.fontSize17]}
                        underlineColorAndroid="transparent"
                        placeholder={'请输入您的真实姓名或工号'}
                        onChangeText={(text)=>{
                            this.setState({
                                name:text
                            })
                        }}
                    ></TextInput>
                </View>
                {/*密码*/}
                <View style={styles.codeView}>
                    <View style={{width: 60, justifyContent:'center'}}>
                        <Text style={[styles.fontSize17]}>密码</Text>
                    </View>
                    <TextInput
                        style={[styles.pwdText,styles.fontSize17]}
                        underlineColorAndroid="transparent"
                        placeholder={'6-20位密码'}
                        secureTextEntry={!this.state.showPwd}
                        onChangeText={(text)=>{
                            this.setState({
                                pwd:text
                            })
                        }}
                    ></TextInput>
                    <TouchableWithoutFeedback onPress={()=>{
                        var show = this.state.showPwd;
                        this.setState({
                            showPwd: !show
                        })
                    }}>

                        <Image
                            style={{width:60}}
                            resizeMode="center"
                            source={(this.state.showPwd == true)?require('../images/Login/visible.png'):require('../images/Login/hidden.png')}
                        ></Image>
                    </TouchableWithoutFeedback>
                </View>
                {/*注册*/}
                {CustomDiv.nextDiv('注册',this._register)}
            </View>
        )
    }
})

const styles = StyleSheet.create({
    fontSize17: {
        fontSize: 17
    },

    getCodeBody: {
        flex: 1,
        backgroundColor: Global.whiteColor
    },
    titleView: {
        paddingTop: 50,
        height: 80,
        marginLeft: 10,
        borderBottomWidth: 1,
        borderBottomColor: Global.lightGrayColor,
        flexDirection: 'row'
    },
    codeView: {
        flexDirection: 'row',
        height: 44,
        marginLeft: 10,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Global.lightGrayColor,
        alignItems: 'center'
    },
    userNameText: {
        marginTop: -10,
        paddingLeft: 15,
        paddingRight: 15,
        flex: 1
    },
    pwdText: {
        paddingLeft: 15,
        paddingRight: 10,
        flex: 1
    },
    next: {
        backgroundColor: Global.themeColor,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15,
        marginTop: 30,
        borderRadius: 6,
    }
})

module.exports = UserSet;