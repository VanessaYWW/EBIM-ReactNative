/**
 * Created by youweiwei on 2017/3/23.
 */
import React, { Component } from 'react';
import {
    Alert,
    Text,
    View,
} from 'react-native';

const Global = require('../Global');

// 点击上面下一步按钮,即同意使用EBIM使用协议
var TipView = React.createClass({
    render(){
        return(
            <View style={{height: 80, paddingLeft: 60, paddingRight: 60, paddingTop: 30,}}>
                <Text style={{textAlign:'center', fontSize: 13}}>点击上面下一步按钮,即同意使用</Text>
                <Text
                    style={{textAlign:'center', fontSize: 13, color: Global.themeColor}}
                    onPress={()=>{
                        Alert.alert('EBIM使用协议')
                    }}>
                    EBIM使用协议
                </Text>
            </View>
        )
    }
});

module.exports = TipView;