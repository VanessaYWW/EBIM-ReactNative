/**
 * Created by youweiwei on 2017/4/1.
 */
import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableOpacity,
    NativeAppEventEmitter
} from 'react-native';

const Global = require('../Global');
const CommonStyle = require('../Common')
const NetWork = require('../Tools/NetWork')
const NavBar = require('../Custom/NavigationBar')
const CustomDiv = require('../Custom/CustomDiv')

module.exports = React.createClass({
    getInitialState(){
        return{
            showPwd: false
        }
    },
    _sure(){
        // console.log(this.props.user)
       NetWork.resetPwd(this.props.user._id,this.pwd,(data)=>{
           console.log('修改成功'+data);
       },(err)=>{})
    },
    render(){
        return(
            <View style={[CommonStyle.flex,{backgroundColor:Global.whiteColor}]}>
                <NavBar title='重设密码'
                        leftImage={require('../images/Common/button_return_white.png')}
                        leftTitle="登录"
                        leftAction={()=>{
                            this.props.navigator.popToTop()
                        }}/>
                <View style={[styles.codeView,CommonStyle.bottomBorder,CommonStyle.alignItemsCenter]}>
                    <View style={{width: 60, justifyContent:'center'}}>
                        <Text style={[CommonStyle.fontSize17]}>密码</Text>
                    </View>
                    <TextInput
                        style={[styles.pwdText,CommonStyle.fontSize17]}
                        placeholder={'6-20位密码'}
                        secureTextEntry={!this.state.showPwd}
                        underlineColorAndroid="transparent"
                        onChangeText={(text)=>{
                            this.pwd = text
                        }}
                    ></TextInput>
                    <TouchableOpacity onPress={()=>{
                        var show = this.state.showPwd;
                        this.setState({
                            showPwd: !show
                        })
                    }}>

                        <Image
                            style={{width:60}}
                            resizeMode="center"
                            source={(this.state.showPwd == true)?require('../images/Login/visible.png'):require('../images/Login/hidden.png')}
                        ></Image>
                    </TouchableOpacity>
                </View>
                {/*确定*/}
                {CustomDiv.nextDiv('确定',this._sure)}
            </View>
        )
    }
})

const styles = StyleSheet.create({
    codeView: {
        flexDirection: 'row',
        height: 44,
        marginLeft: 10,
        marginTop:20
    },
    pwdText: {
        paddingLeft: 15,
        paddingRight: 10,
        flex: 1
    }
})