/**
 * Created by youweiwei on 2017/4/1.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    Navigator,
} from 'react-native';

const Register = require('./Register')

// 注册第一步,手机号+图片验证码
export default MainView = React.createClass({
    /**
     * 配置场景动画
     * @param route 路由
     * @param routeStack 路由栈
     * @returns {*} 动画
     */

    configureScene() {
        return Navigator.SceneConfigs.PushFromRight; // 右侧弹出
    },
    /**
     * 使用动态页面加载
     * @param route 路由
     * @param navigator 导航器
     * @returns {XML} 页面
     */
    renderScene(route, navigator) {
        return <route.component navigator={navigator}  {...route.passProps} />;
    },
    componentWillUnmount(){
    },

    render() {
        return (
            <Navigator
                style={{flex:1}}
                initialRoute={{
                    component: Register,
                    passProps:{
                        type:'regist'
                    }
                }}
                configureScene={this.configureScene}
                renderScene={this.renderScene}/>
        );
    }
})


AppRegistry.registerComponent('EBIM', () => MainView);
