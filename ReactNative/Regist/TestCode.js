/**
 * Created by youweiwei on 2017/3/23.
 * 发送验证码->输入验证码
 */
import React, { Component } from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableWithoutFeedback,
    ScrollView,
    Navigator,
    Picker,
    NativeAppEventEmitter
} from 'react-native';

const Global = require('../Global');
const NetWork = require('../Tools/NetWork')
const NavBar = require('../Custom/NavigationBar')
const UserSet = require('./UserSet')
const TipView = require('./TipView')
const ChangePwd = require('./ChangePwd')
const CustomDiv = require('../Custom/CustomDiv')

// 2.输入验证码
var TestCode = React.createClass({
    getInitialState(){
        return {
            time:10,
        }
    },
    // 获取验证码
    _getNewCode: function () {
        // 发送获取验证码网络请求
        NetWork.postMobileVerify(this.props.phone,(data)=>{
            console.log(data.hash);
            this.hash = data.hash;
        },(error)=>{
            console.log(error);
        })

        // 开启倒计时
        this._timerWork();
    },
    // 开启倒计时
    _timerWork: function () {
        var _that = this;
        var curTime = new Number(10);
        setInterval(function () {
            if (curTime > 0){
                curTime--;
                _that.setState({
                    time:curTime,
                })
            }
        }, 1000);

    },
    // 点击下一步
    nextStep:function () {
        if (this.state.codeText == this.hash){
            if (this.props.type == 'regist'){//注册
                this.props.navigator.push({
                    component: UserSet,
                    passProps: {
                        name: '注册',
                        phone:this.props.phone
                    },
                })
            }else {//找回密码
                this.props.navigator.push({
                    component: ChangePwd,
                    passProps: {
                        phone:this.props.phone,
                        user:this.props.user
                    },
                })
            }
        }
    },
    // 处理手机号
    _encipher:function () {
        var newPhone = new String();
        var frist = this.props.phone.slice(0, 3);
        var last = this.props.phone.slice(-4, 11);
        newPhone= frist+'****'+last;
        return newPhone;
    },

    componentWillMount(){
        this._getNewCode();
    },
    render(){
        var newCode = this._encipher();
        return(
            <View style={styles.getCodeBody}>
                <NavBar
                    title={this.props.name}
                    leftImage={require('../images/Common/button_return_white.png')}
                    leftTitle="登录"
                    leftAction={()=>{
                        this.props.navigator.popToTop()
                    }}/>
                <View style={styles.titleView}>
                    <Text>我们已经向</Text>
                    <Text style={{color:Global.themeColor}}>{newCode}</Text>
                    <Text>发送验证码</Text>
                </View>
                <View style={styles.codeView}>
                    <View style={{width: 60, justifyContent:'center'}}>
                        <Text style={[styles.fontSize17]}>验证码</Text>
                    </View>
                    <TextInput
                        underlineColorAndroid="transparent"
                        style={{flexDirection:'row', flex:1,marginLeft: 15,  alignItems:'center', justifyContent:'center'}}
                        placeholder={'请输入4位验证码'} onChangeText={(text)=>{
                        this.setState({
                            codeText:text
                        })
                    }}></TextInput>
                    <View style={styles.timeView} >
                        <Text style={{textAlign:'center'}} onPress={this.state.time==0?this._getNewCode.bind(null,this):null}>{
                            this.state.time==0?'获取验证码':this.state.time
                        }</Text>
                        {
                            this.state.time==0?null:
                                <Text style={{textAlign:'center'}}>S后重发</Text>

                        }
                    </View>
                </View>
                {/*下一步*/}
                {CustomDiv.nextDiv('下一步',this.nextStep)}
                {/*协议*/}
                {this.props.type=='regist'?<TipView></TipView>:null}
            </View>
        )
    }
})




const styles = StyleSheet.create({
    fontSize17: {
        fontSize: 17
    },

    getCodeBody: {
        flex: 1,
        backgroundColor: Global.whiteColor
    },
    titleView: {
        paddingTop: 50,
        height: 80,
        marginLeft: 10,
        borderBottomWidth: 1,
        borderBottomColor: Global.lightGrayColor,
        flexDirection: 'row'
    },
    codeView: {
        flexDirection: 'row',
        height: 44,
        marginLeft: 10,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Global.lightGrayColor,
        alignItems: 'center'
    },
    timeView: {
        flexDirection: 'row',
        width: 100,
        paddingRight: 20,
        borderLeftWidth: 1,
        borderLeftColor: Global.lightGrayColor,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    next: {
        backgroundColor: Global.themeColor,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15,
        marginTop: 30,
        borderRadius: 6,
    }
});

module.exports = TestCode;