/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import { StackNavigator } from 'react-navigation';

import ScreenOne from './ScreenOne'
import ScreenTwo from './ScreenTwo'

class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Welcome',
    };
    render() {
        const { params } = this.props.navigation.state;
        const age = this.props['age'];
        return <Text onPress={()=>this.props.navigation.navigate('One')}>{params.age}</Text>;
    }
}

const SimpleApp = StackNavigator({
    Home: { screen: HomeScreen },
    One:{screen:ScreenOne},
    Two:{screen:ScreenTwo},
});

AppRegistry.registerComponent('EBIM', () => SimpleApp);
